'use strict'
const express = require ('express')
const bodyParser = require('body-parser') 
const cors = require('cors')
const {Pool} = require('pg')

const jwt = require('jsonwebtoken')






const app = express()
const port = process.env.port || 3001

//LA CONEXION A LA BASE DE DATOS
const pool = new Pool({
    user : 'postgres',
    host : 'localhost',
    password : 'postgresql',
    database : 'mundial2014',
    port : '5432'
})






app.use(bodyParser.urlencoded({ extended : false }))
app.use(bodyParser.json())
app.use(cors({ origin: 'http://localhost:4200' }))


var unirest = require("unirest");
//Paises
var req = unirest("GET", "https://montanaflynn-fifa-world-cup.p.rapidapi.com/teams") 
//Rondas
var req1 = unirest("GET", "https://montanaflynn-fifa-world-cup.p.rapidapi.com/rounds")
//Juegos
var reqGames = unirest("GET", "https://montanaflynn-fifa-world-cup.p.rapidapi.com/games")
// Personas
var reqPerson = unirest("GET", "https://montanaflynn-fifa-world-cup.p.rapidapi.com/persons")
// Goles
var reqGoals = unirest("GET", "https://montanaflynn-fifa-world-cup.p.rapidapi.com/goals")

const userPool = new Pool ({
    email : String,
    password: String
})

module.exports =  userPool


app.get('/usuari', (req, res) =>{
    const createTable = async () =>{
        const response = await pool.query('CREATE TABLE usuarios(email text, password text)')
        //console.log(response)
        res.send("La tabla ha sido creada")
        
        
    }

    createTable()
})

app.post('/singup', (req,res) =>{
    const createUser = async () =>{
        const {email, password} = req.body
        const response = await pool.query('INSERT INTO usuarios(email, password) VALUES ($1, $2)', [email, password])
        console.log(response)
        const token = jwt.sign({email: email}, 'secretKey')
        res.json(token ) 

    }

    createUser()
})

app.post('/singin', (req, res) => {
    const singUser = async () => {
        const {email, password} = req.body

        const user = await pool.query('SELECT * FROM usuarios WHERE email = $1', [ email ])
        
        if(!user) return res.status(401).send("El correo no existe") 
        var user1 = user.rows
        for (var i = 0; i < user1.length; i++){
            if(user1[i].password !== password) return res.status(401).send("contraseña erronea")
        } 
        const token = jwt.sign({email: email}, 'secretKey')
        res.json(token)

    }

    singUser()
})


app.get('/private-task', verifyToken, (req, res) => {
    res.json([
        {
            id:1,
            name: "probando"

        },
        {
            id:2,
            name: "seguimos probando"
        }
    ])
})


function verifyToken(req, res, next) {
    if(!req.headers.authorization){
        return res.status(401).send('Unauthorized request')
    }
    const token = req.headers.authorization.split(' ')[1]
    console.log(token)
    if (token === null){
        return res.status(401).send('Unauthorized request')
    }
   
    const payload = jwt.verify(token, 'secretKey')
    console.log(payload)

    req.userId = payload.email 
    next();

}


  


//MOSTRAR LOS PAISES Y CUALES PARTICIPARON DEL MUNDIAL
req.headers({
	"x-rapidapi-host": "montanaflynn-fifa-world-cup.p.rapidapi.com",
	"x-rapidapi-key": "da21fce768mshc2414d78b346310p12fdcbjsnccff2820202d",
	"accepts": "json"
})

req.end(function (res)  {
    var arr1 = []
    var paises = []
	if (res.error) throw new Error(res.error)
    //console.log(res.body)
    arr1 = res.body
    var primero = arr1[0]
    
    //console.log(arr1)

    //Para agregar los paises a la base de datos
    app.get('/paises', (req, res) =>{
        var africa = arr1.slice(0,55)
        var asia = arr1.slice(56,88)
        var NorteAmerica = arr1.slice(89,122)
        var europa = arr1.slice(123, 174)
        var asia1 = arr1.slice(175, 188)
        var NorteAmerica1= arr1.slice(189,191)
        var oceania = arr1.slice(192, 208)
        var latinoAmerica = arr1.slice(209, 220)

        res.json('organizando continentes')
        var pais = europa  
        for (var i = 0; i < pais.length; i++){
            //CREA UN REGISTRO EN LA BASE DE DATOS
            const insertUser = async () => {
              
            const text = 'INSERT INTO paises1(id, key, title, code, country_id, national, created_at, updated_at, continent) VALUES($1, $2, $3, $4, $5, $6, $7, $8, $9)'
            const values = [pais[i].id,pais[i].key,pais[i].title,pais[i].code,pais[i].country_id, pais[i].national, pais[i].created_at, pais[i].updated_at, 'Europa']
            
            const res = await pool.query(text, values)    
            
            console.log(res)
            console.log(values)
            } 
      
            insertUser()    
        }
    })


    //Para mostrar todos los paises del mundo
    app.get('/paises/listar', (req, res) =>{
        const getPaises = async () => {
            try{
                const response = await pool.query('SELECT array_to_json(array_agg(p.*)) FROM paises1 p')
                //console.log(response.rows)
                console.log('funciona')
                res.json(response.rows)
                paises = response.rows
                
            } catch(e){
                console.log(e)
            }   
        }   
    getPaises()     
    }) 
    
    //Para buscar los paises por cada continente
    app.get('/paises/listar/:continent', (req, res) =>{
        const getPaisByCont = async () => {
            const continent = req.params.continent
            const response =  await pool.query('SELECT * FROM paises1 WHERE continent = $1', [ continent ])
            //const name = req.params.name
            //const response =  await pool.query('SELECT * FROM users WHERE name = $1', [ name ])
            res.json(response.rows)
        }
    
        getPaisByCont()

    })
    

    //Para saber si un pais participo del mundial
    //Mostrar los equipos que participaron del mundial, organizados por su continente
    //Y en orden alfabetico
    app.get('/paises/mundial', (req, res) => {
        const getPaisesMundial = async () => {
            
            const selecciones = ['BRA', 'CRO', 'MEX', 'CMR', 'ESP', 'NED', 'CHI', 'AUT', 'COL', 'GRE', 'CIV', 'JPN', 'URU', 'CRC', 'ENG', 'ITA', 'SUI', 'ECU', 'FRA', 'HON', 'ARG', 'BIH', 'IRN', 'NGA', 'GER', 'POR', 'GHA', 'USA', 'BEL', 'ALG', 'RUS', 'KOR']
            var arrSelec = []
            var arrAmerica = [], arrAfrica = [], arrAsia = [], arrEuropa = [], arrOceania = []
            for (var i = 0; i < selecciones.length; i++) { 
                const response =  await pool.query('SELECT * FROM paises1 WHERE code = $1' , [ selecciones[i] ])
                //var selec = response.rows
                arrSelec.push(response.rows)
                //console.log(selec)
                if (arrSelec[i][0].continent == 'America'){
                    arrAmerica.push([arrSelec[i][0].title,arrSelec[i][0].key, arrSelec[i][0].id ])
                } else if (arrSelec[i][0].continent == 'Africa'){
                    arrAfrica.push([arrSelec[i][0].title, arrSelec[i][0].key, arrSelec[i][0].id])
                } else if (arrSelec[i][0].continent == 'Asia'){
                    arrAsia.push([arrSelec[i][0].title, arrSelec[i][0].key, arrSelec[i][0].id])
                } else if (arrSelec[i][0].continent == 'Europa'){
                    arrEuropa.push([arrSelec[i][0].title, arrSelec[i][0].key, arrSelec[i][0].id])
                }else if (arrSelec[i][0].continent == 'Oceania'){
                    arrOceania.push([arrSelec[i][0].title, arrSelec[i][0].key, arrSelec[i][0].id])
                }

                    
                //console.log(arrAmerica)
                //console.log(arrSelec[i][0].title)

            }   
            var arrContinentes = [arrAmerica.sort(), arrAfrica.sort(), arrAsia.sort(), arrEuropa.sort(), arrOceania.sort()]
            res.json(arrContinentes)
            
        }
        
        getPaisesMundial()
    })
})

//PARA LISTAR LOS NOMBRES DE LOS PARTIDOS Y SU FECHA 
req1.headers({
	"x-rapidapi-host": "montanaflynn-fifa-world-cup.p.rapidapi.com",
	"x-rapidapi-key": "da21fce768mshc2414d78b346310p12fdcbjsnccff2820202d",
	"accepts": "json"
})

req1.end(function (res) {
    if (res.error) throw new Error(res.error);
    var arrRounds = []
    arrRounds = res.body

    //PARA INSERTAR LAS RONDAS A LA TABLA ROUNDS
    app.get('/rounds', (req, res) => {
        for(var i = 0; i < arrRounds.length ; i++){
            const insertRounds = async () => {
                const text = 'INSERT INTO rounds(id, title, pos, knockout, start_at, end_at, auto, created_at, updated_at, round_id) VALUES($1, $2, $3, $4, $5, $6, $7, $8, $9, $10)'
                const values = [arrRounds[i].id, arrRounds[i].title, arrRounds[i].pos, arrRounds[i].knockout, arrRounds[i].start_at, arrRounds[i].end_at, arrRounds[i].auto, arrRounds[i].created_at, arrRounds[i].updated_at, arrRounds[i].pos]
                
                var response = await pool.query(text, values)
                
                console.log(response)
                console.log(values)


            }
            insertRounds()
        }
    })

    //PARA MOSTRAR LAS RONDAS 
    app.get('/listar/rounds', (req, res) => {
        const getListaRounds = async () =>{
            var arrListaRounds = []
            const response = await pool.query('SELECT * FROM rounds')
            arrListaRounds.push(response.rows)
            

            for (var c = 0; c < arrListaRounds[0].length; c++){
                //console.log(arrListaRounds[0][c].title, arrListaRounds[0][c].start_at)
                var arrListaRounds1 = [arrListaRounds[0][c].title,arrListaRounds[0][c].start_at] 
            }
            res.json(arrListaRounds1)
        }
        getListaRounds()

    })
    
	//console.log(arrRounds)
})

//PARA MOSTRAR LOS JUEGOS Y LAS TABLAS DE POSICIONES
reqGames.headers({
	"x-rapidapi-host": "montanaflynn-fifa-world-cup.p.rapidapi.com",
	"x-rapidapi-key": "da21fce768mshc2414d78b346310p12fdcbjsnccff2820202d",
	"accepts": "json"
});

reqGames.end(function (res) {
    var arrGames = []
    if (res.error) throw new Error(res.error)
    arrGames = res.body
    //PARA INSERTAR LOS JUEGOS A LA BASE DE DATOS
    app.get('/insertar/games', (req, res) => {
        for (var i = 0; i < arrGames.length; i++){
            const insertGames = async() => {
                const text = 'INSERT INTO games1(id, round_id, pos, team1_id, team2_id, play_at, postponed, knockout, home, score1, score2, winner, winner90, created_at, updated_at, start_at, group_id) VALUES($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15, $16, $17)'
                const values = [arrGames[i].id, arrGames[i].round_id, arrGames[i].pos, arrGames[i].team1_id, arrGames[i].team2_id, arrGames[i].play_at, arrGames[i].postponed, arrGames[i].knockout, arrGames[i].home, arrGames[i].score1, arrGames[i].score2, arrGames[i].winner, arrGames[i].winner90, arrGames[i].created_at, arrGames[i].updated_at, arrGames[i].play_at, arrGames[i].group_id] 

                var response = await pool.query(text, values)
                
                console.log(response)
                console.log(values)
            }

            insertGames()
        }
        

    })
    
    //PARA MOSTRAR LOS PARTIDOS CON SU RESULTADO
    app.get('/listar/games', (req, res) => {
        const getPartidos = async () => {
            var text = 'SELECT rounds.title, games1.play_at, games1.team1_id, games1.team2_id, games1.score1, games1.score2, games1.winner, games1.group_id FROM rounds INNER JOIN games1 ON rounds.round_id=games1.round_id ORDER BY rounds.title'

            var response = await pool.query(text) 
            res.json(response.rows)
            
        }

        getPartidos()

    })

    //PARA MOSTRAR LA TABLA DE POSICIONES 
    app.get('/tabla/resultados', (req, res) => {
        const getPartidos = async () => {
            var text = 'SELECT rounds.title, games1.play_at, games1.team1_id, games1.team2_id, games1.score1, games1.score2, games1.winner, games1.group_id, rounds.title FROM rounds INNER JOIN games1 ON rounds.round_id=games1.round_id ORDER BY rounds.title'

            var response = await pool.query(text)
            var grupos = response.rows
            var BRA = 0, CRO = 0, MEX = 0, CAM = 0, ESP= 0, NED= 0, CHI= 0, AUT= 0, COL= 0, GRE= 0, CIV= 0, JPN= 0, URU= 0, CRC= 0, ENG= 0, ITA= 0, SUI= 0, ECU= 0, FRA= 0, HON= 0, ARG= 0, BIH= 0, IRN= 0, NGA= 0, GER= 0, POR= 0, GHA= 0, USA= 0, BEL= 0, ALG= 0, RUS= 0, KOR = 0
            var gfBra = 0, gfCro = 0, gfMex = 0, gfCam = 0, gfEsp = 0, gfNed = 0, gfChi = 0, gfAut = 0, gfCol = 0, gfGre = 0, gfCiv = 0, gfJPN = 0, gfUru = 0, gfCrc = 0, gfEng = 0, gfIta = 0, gfSui = 0, gfEcu = 0, gfFra = 0, gfHon = 0, gfArg = 0, gfBih = 0, gfIrn = 0, gfNga = 0, gfGer = 0, gfPor = 0, gfGha = 0, gfUsa = 0, gfBel = 0, gfAlg = 0, gfRus = 0, gfKor = 0
            var gcBra = 0, gcCro = 0, gcMex = 0, gcCam = 0, gcEsp = 0, gcNed = 0, gcChi = 0, gcAut = 0, gcCol = 0, gcGre = 0, gcCiv = 0, gcJPN = 0, gcUru = 0, gcCrc = 0, gcEng = 0, gcIta = 0, gcSui = 0, gcEcu = 0, gcFra = 0, gcHon = 0, gcArg = 0, gcBih = 0, gcIrn = 0, gcNga = 0, gcGer = 0, gcPor = 0, gcGha = 0, gcUsa = 0, gcBel = 0, gcAlg = 0, gcRus = 0, gcCor = 0
            const selecciones = ['BRA', 'CRO', 'MEX', 'CMR', 'ESP', 'NED', 'CHI', 'AUT', 'COL', 'GRE', 'CIV', 'JPN', 'URU', 'CRC', 'ENG', 'ITA', 'SUI', 'ECU', 'FRA', 'HON', 'ARG', 'BIH', 'IRN', 'NGA', 'GER', 'POR', 'GHA', 'USA', 'BEL', 'ALG', 'RUS', 'KOR']
            var resultA = [], resultB = [], resultC = [], resultD = [], resultE= [], resultF = [], resultG = [], resultH = []
            
            for (var i = 0; i < grupos.length; i++){
                if (grupos[i].group_id == '1'){
                    resultA.push([grupos[i].team1_id, grupos[i].team2_id, grupos[i].score1, grupos[i].score2, grupos[i].winner, grupos[i].title])
                }else if (grupos[i].group_id == '2') {
                    resultB.push([grupos[i].team1_id, grupos[i].team2_id, grupos[i].score1, grupos[i].score2, grupos[i].winner, grupos[i].title])
                }else if (grupos[i].group_id == '3') {
                    resultC.push([grupos[i].team1_id, grupos[i].team2_id, grupos[i].score1, grupos[i].score2, grupos[i].winner, grupos[i].title])
                }else if (grupos[i].group_id == '4') {
                    resultD.push([grupos[i].team1_id, grupos[i].team2_id, grupos[i].score1, grupos[i].score2, grupos[i].winner, grupos[i].title])
                }else if (grupos[i].group_id == '5') {
                    resultE.push([grupos[i].team1_id, grupos[i].team2_id, grupos[i].score1, grupos[i].score2, grupos[i].winner, grupos[i].title])
                }else if (grupos[i].group_id == '6') {
                    resultF.push([grupos[i].team1_id, grupos[i].team2_id, grupos[i].score1, grupos[i].score2, grupos[i].winner, grupos[i].title])
                }else if (grupos[i].group_id == '7') {
                    resultG.push([grupos[i].team1_id, grupos[i].team2_id, grupos[i].score1, grupos[i].score2, grupos[i].winner, grupos[i].title])
                }else if (grupos[i].group_id == '8') {
                    resultH.push([grupos[i].team1_id, grupos[i].team2_id, grupos[i].score1, grupos[i].score2, grupos[i].winner, grupos[i].title])
                }           
            }


            for (var c = 0; c < resultA.length; c++){
               
                if(resultA[c][0]== 211){
                    gfBra = gfBra + resultA[c][2]
                    gcBra = gcBra + resultA[c][3]
                }else if (resultA[c][1]== 211){
                    gfBra = gfBra + resultA[c][3]
                    gcBra = gcBra + resultA[c][2]
                }
                if(resultA[c][0]== 154){
                    gfCro = gfCro + resultA[c][2]
                    gcCro = gcCro + resultA[c][3]
                }else if (resultA[c][1]== 154){
                    gfCro = gfCro + resultA[c][3]
                    gcCro = gcCro + resultA[c][2]
                }
                if(resultA[c][0]== 190){
                    gfMex = gfMex + resultA[c][2]
                    gcMex = gcMex + resultA[c][3]
                }else if (resultA[c][1]== 190){
                    gfMex = gfMex + resultA[c][3]
                    gcMex = gcMex + resultA[c][2]
                }
                if(resultA[c][0]== 22){
                    gfCam = gfCam + resultA[c][2]
                    gcCam = gcCam + resultA[c][3]
                }else if (resultA[c][1]== 22){
                    gfCam = gfCam + resultA[c][3]
                    gcCam = gcCam + resultA[c][2]
                }


                if(resultA[c][4] == 1){
                    if(resultA[c][0]== 211){
                        BRA = BRA + 3  
                        
                    }else if (resultA[c][0]== 154){
                        CRO = CRO + 3
                    }else if (resultA[c][0]== 190){
                        MEX = MEX + 3
                    }else if (resultA[c][0]== 22){
                        CAM = CAM + 3
                    }    
                }else if (resultA[c][4] == 2){
                    if(resultA[c][1]== 211){
                        BRA = BRA + 3 
                    } else if (resultA[c][1]== 154){
                        CRO = CRO + 3
                    }else if (resultA[c][1]== 190){
                        MEX = MEX + 3
                    }else if (resultA[c][1]== 22){
                        CAM = CAM + 3
                    }  

                }else if (resultA[c][4] == 0){
                    if(resultA[c][1]== 211 || resultA[c][0]== 211 ){
                        BRA = BRA + 1  
                        
                    }if (resultA[c][0]== 154 || resultA[c][1]== 154){
                        CRO = CRO + 1

                    }if (resultA[c][1]== 190 || resultA[c][0]== 190){
                        MEX = MEX + 1
                    }if (resultA[c][1]== 22 || resultA[c][0]== 22){
                        CAM = CAM + 1
                    }
                }
                
            }
            for (var a = 0; a < resultB.length; a++){
               
                if(resultB[a][0]== 129){
                    gfEsp = gfEsp + resultB[a][2]
                    gcEsp = gcEsp + resultB[a][3]
                }else if (resultB[a][1]== 129 ){
                    gfEsp = gfEsp + resultB[a][3]
                    gcEsp = gcEsp + resultB[a][2]
                }
                if(resultB[a][0]== 137){
                    gfNed = gfNed + resultB[a][2]
                    gcNed = gcNed + resultB[a][3]
                }else if (resultB[a][1]== 137){
                    gfNed = gfNed + resultB[a][3]
                    gcNed = gcNed + resultB[a][2]
                }
                if(resultB[a][0]== 212){
                    gfChi = gfChi + resultB[a][2]
                    gcChi = gcChi + resultB[a][3]
                }else if (resultB[a][1]== 212){
                    gfChi = gfChi + resultB[a][3]
                    gcChi = gcChi + resultB[a][2]
                }
                
                if(resultB[a][0]== 124){
                    gfAut = gfAut + resultB[a][2]
                    gcAut = gcAut + resultB[a][3]
                }if (resultB[a][1]== 124){
                    gfAut = gfAut + resultB[a][3]
                    gcAut = gcAut + resultB[a][2]
                }

                if(resultB[a][4] == 1){
                    if(resultB[a][0]== 129){
                        ESP =ESP  + 3  
                        
                    }else if (resultB[a][0]==137 ){
                        NED = NED + 3
                    }else if (resultB[a][0]== 212){
                        CHI = CHI + 3
                    }else if (resultB[a][0]== 124 ){
                        AUT = AUT + 3
                    }    
                }else if (resultB[a][4] == 2){
                    if(resultB[a][1]== 129){
                        ESP = ESP + 3 
                    } else if (resultB[a][1]== 137){
                        NED =  NED+ 3
                    }else if (resultB[a][1]== 212){
                        CHI = CHI + 3
                    }else if (resultB[a][1]== 124){
                        AUT = AUT + 3
                    }  

                }else if (resultB[a][4] == 0){
                    if(resultB[a][1]== 129  || resultB[a][0]== 129  ){
                        ESP =  ESP+ 1  
                        
                    }if (resultB[a][0]== 137 || resultB[a][1]== 137){
                        NED = NED + 1

                    }if (resultB[a][1]== 212  || resultB[a][0]== 212 ){
                        CHI = CHI + 1
                    }if (resultB[a][1]== 124 || resultB[a][0]== 124){
                        AUT = AUT + 1
                    }
                }
                
            }
            for (var b = 0; b < resultC.length; b++){
               
                if(resultC[b][0]== 215){
                    gfCol = gfCol + resultC[b][2]
                    gcCol = gcCol + resultC[b][3]
                }else if (resultC[b][1]== 215 ){
                    gfCol = gfCol + resultC[b][3]
                    gcCol = gcCol + resultC[b][2]
                }
                if(resultC[b][0]==132 ){
                    gfGre = gfGre + resultC[b][2]
                    gcGre = gcGre + resultC[b][3]
                }else if (resultC[b][1]== 132){
                    gfGre = gfGre + resultC[b][3]
                    gcGre = gcGre + resultC[b][2]
                }
                if(resultC[b][0]== 17){
                    gfCiv = gfCiv + resultC[b][2]
                    gcCiv = gcCiv + resultC[b][3]
                }else if (resultC[b][1]== 17 ){
                    gfCiv = gfCiv + resultC[b][3]
                    gcCiv = gcCiv + resultC[b][2]
                }
                if(resultC[b][0]== 72){
                    gfJPN = gfJPN + resultC[b][2]
                    gcJPN = gcJPN + resultC[b][3]
                }else if (resultC[b][1]== 72 ){
                    gfJPN = gfJPN + resultC[b][3]
                    gcJPN = gcJPN + resultC[b][2]
                }

                if(resultC[b][4] == 1){
                    if(resultC[b][0]== 215){
                        COL =  COL+ 3  
                        
                    }else if (resultC[b][0]== 132){
                        GRE = GRE + 3
                    }else if (resultC[b][0]== 17){
                        CIV = CIV  + 3
                    }else if (resultC[b][0]== 72){
                        JPN = JPN + 3
                    }    
                }else if (resultC[b][4] == 2){
                    if(resultC[b][1]== 215 ){
                        COL = COL + 3 
                    } else if (resultC[b][1]== 132){
                        GRE = GRE + 3
                    }else if (resultC[b][1]== 17){
                        CIV = CIV + 3
                    }else if (resultC[b][1]== 72){
                        JPN = JPN + 3
                    }  

                }else if (resultC[b][4] == 0){
                    if(resultC[b][1]==  215 || resultC[b][0]== 215){
                        COL = COL  + 1  
                        
                    }if (resultC[b][0]== 132 || resultC[b][1]== 132 ){
                        GRE = GRE + 1

                    }if (resultC[b][1]== 17  || resultC[b][0]== 17 ){
                        CIV = CIV + 1

                    }if (resultC[b][1]== 72 || resultC[b][0]== 72){
                        JPN = JPN + 1
                    }
                }
                
            }
            for (var d = 0; d < resultD.length; d++){

                if(resultD[d][0]== 118){
                    gfCrc = gfCrc + resultD[d][2]
                    gcCrc = gcCrc + resultD[d][3]
                }else if (resultD[d][1]== 118 ){
                    gfCrc = gfCrc + resultD[d][3]
                    gcCrc = gcCrc + resultD[d][2]
                }
                if(resultD[d][0]== 214){
                    gfUru = gfUru + resultD[d][2]
                    gcUru = gcUru + resultD[d][3]
                }else if (resultD[d][1]== 214 ){
                    gfUru = gfUru + resultD[d][3]
                    gcUru = gcUru + resultD[d][2]
                }
                if(resultD[d][0]== 134){
                    gfIta = gfIta + resultD[d][2]
                    gcIta = gcIta + resultD[d][3]
                }else if (resultD[d][1]== 134){
                    gfIta = gfIta + resultD[d][3]
                    gcIta = gcIta + resultD[d][2]
                }
                if(resultD[d][0]== 170){
                    gfEng = gfEng + resultD[d][2]
                    gcEng = gcEng + resultD[d][3]
                }else if (resultD[d][1]== 170){
                    gfEng = gfEng + resultD[d][3]
                    gcEng = gcEng + resultD[d][2]
                }

                if(resultD[d][4] == 1){
                    if(resultD[d][0]== 118){
                        CRC = CRC + 3  
                        
                    }else if (resultD[d][0]== 214){
                        URU = URU + 3
                    }else if (resultD[d][0]== 134){
                        ITA = ITA + 3
                    }else if (resultD[d][0]== 170){
                        ENG = ENG + 3
                    }    
                }else if (resultD[d][4] == 2){
                    if(resultD[d][1]== 118){
                        CRC = CRC + 3 
                    } else if (resultD[d][1]== 214 ){
                        URU = URU + 3
                    }else if (resultD[d][1]== 134){
                        ITA = ITA + 3
                    }else if (resultD[d][1]== 170){
                        ENG = ENG + 3
                    }  

                }else if (resultD[d][4] == 0){
                    if(resultD[d][1]== 118 || resultD[d][0]== 118 ){
                        CRC = CRC + 1  
                        
                    }if (resultD[d][0]== 214  || resultD[d][1]==  214){
                        URU = URU + 1

                    }if (resultD[d][1]== 134  || resultD[d][0]==  134){
                        ITA = ITA + 1
                    }if (resultD[d][1]== 170 || resultD[d][0]== 170 ){
                        ENG = ENG + 1
                    }
                }
               
                
            }
            for (var e = 0; e < resultE.length; e++){
                
                if(resultE[e][0]== 131){
                    gfFra = gfFra + resultE[e][2]
                    gcFra = gcFra + resultE[e][3]
                }else if (resultE[e][1]== 131 ){
                    gfFra = gfFra + resultE[e][3]
                    gcFra = gcFra + resultE[e][2]
                }
                if(resultE[e][0]== 153 ){
                    gfSui = gfSui + resultE[e][2]
                    gcSui = gcSui + resultE[e][3]
                }else if (resultE[e][1]== 153){
                    gfSui = gfSui + resultE[e][3]
                    gcSui = gcSui + resultE[e][2]
                }
                if(resultE[e][0]== 216){
                    gfEcu = gfEcu + resultE[e][2]
                    gcEcu = gcEcu + resultE[e][3]
                }else if (resultE[e][1]== 216){
                    gfEcu = gfEcu + resultE[e][3]
                    gcEcu = gcEcu + resultE[e][2]
                }
                if(resultE[e][0]== 117 ){
                    gfHon = gfHon + resultE[e][2]
                    gcHon = gcHon + resultE[e][3]
                }else if (resultE[e][1]== 117){
                    gfHon = gfHon + resultE[e][3]
                    gcHon = gcHon + resultE[e][2]
                }

                if(resultE[e][4] == 1){
                    if(resultE[e][0]== 131 ){
                        FRA = FRA + 3  
                        
                    }else if (resultE[e][0]== 153){
                        SUI = SUI + 3
                    }else if (resultE[e][0]== 216){
                        ECU = ECU + 3
                    }else if (resultE[e][0]== 117){
                        HON = HON + 3
                    }    
                }else if (resultE[e][4] == 2){
                    if(resultE[e][1]== 131){
                        FRA = FRA + 3 
                    } else if (resultE[e][1]== 153 ){
                        SUI = SUI + 3
                    }else if (resultE[e][1]== 216 ){
                        ECU = ECU + 3
                    }else if (resultE[e][1]== 117){
                        HON = HON + 3
                    }  

                }else if (resultE[e][4] == 0){
                    if(resultE[e][1]== 131  || resultE[e][0]== 131){
                        FRA = FRA + 1  
                        
                    }if (resultE[e][0]== 153  || resultE[e][1] == 153  ){
                        SUI = SUI+ 1

                    }if (resultE[e][1]== 216  || resultE[e][0]== 216 ){
                        ECU = ECU + 1
                    }if (resultE[e][1]== 117 || resultE[e][0]== 117 ){
                        HON = HON + 1
                    }
                }
                
                
            }
            for (var f = 0; f < resultF.length; f++){
                
                
                if(resultF[f][0]== 210){
                    gfArg = gfArg + resultF[f][2]
                    gcArg = gcArg + resultF[f][3]
                }else if (resultF[f][1]==210 ){
                    gfArg = gfArg + resultF[f][3]
                    gcArg = gcArg + resultF[f][2]
                }
                if(resultF[f][0]== 20){
                    gfNga = gfNga+ resultF[f][2]
                    gcNga = gcNga + resultF[f][3]
                }else if (resultF[f][1]==20 ){
                    gfNga = gfNga + resultF[f][3]
                    gcNga = gcNga + resultF[f][2]
                }
                if(resultF[f][0]== 162){
                    gfBih = gfBih + resultF[f][2]
                    gcBih = gcBih + resultF[f][3]
                }else if (resultF[f][1]== 162 ){
                    gfBih = gfBih + resultF[f][3]
                    gcBih = gcBih + resultF[f][2]
                }
                if(resultF[f][0]== 178){
                    gfIrn = gfIrn + resultF[f][2]
                    gcIrn = gcIrn + resultF[f][3]
                }else if (resultF[f][1]== 178 ){
                    gfIrn = gfIrn + resultF[f][3]
                    gcIrn = gcIrn + resultF[f][2]
                }

                if(resultF[f][4] == 1){
                    if(resultF[f][0]== 210){
                        ARG = ARG + 3  
                        
                    }else if (resultF[f][0]== 20){
                        NGA = NGA + 3
                    }else if (resultF[f][0]== 162){
                        BIH = BIH + 3
                    }else if (resultF[f][0]== 178){
                        IRN = IRN + 3
                    }    
                }else if (resultF[f][4] == 2){
                    if(resultF[f][1]== 210){
                        ARG = ARG + 3 
                    } else if (resultF[f][1]== 20){
                        NGA = NGA + 3
                    }else if (resultF[f][1]== 162){
                        BIH = BIH + 3
                    }else if (resultF[f][1]== 178){
                        IRN = IRN + 3
                    }  

                }else if (resultF[f][4] == 0){
                    if(resultF[f][1]==  210 || resultF[f][0]==  210 ){
                        ARG = ARG + 1  
                        
                    }if (resultF[f][0]== 20   || resultF[f][1]== 20 ){
                        NGA = NGA + 1

                    }if (resultF[f][1]== 162  || resultF[f][0]== 162 ){
                        BIH = BIH + 1
                    }if (resultF[f][1]==  178 || resultF[f][0]== 178){
                        IRN = IRN + 1
                    }
                }
                
                
            }
            for (var g = 0; g < resultG.length; g++){
               
                
                if(resultG[g][0]== 127 ){
                    gfGer = gfGer + resultG[g][2]
                    gcGer = gcGer + resultG[g][3]
                }else if (resultG[g][1]== 127 ){
                    gfGer = gfGer + resultG[g][3]
                    gcGer = gcGer + resultG[g][2]
                }
                if(resultG[g][0]== 191){
                    gfUsa = gfUsa + resultG[g][2]
                    gcUsa = gcUsa + resultG[g][3]
                }else if (resultG[g][1]== 191){
                    gfUsa = gfUsa + resultG[g][3]
                    gcUsa = gcUsa + resultG[g][2]
                }
                if(resultG[g][0]== 138){
                    gfPor = gfPor + resultG[g][2]
                    gcPor = gcPor + resultG[g][3]
                }else if (resultG[g][1]== 138 ){
                    gfPor = gfPor + resultG[g][3]
                    gcPor = gcPor + resultG[g][2]
                }
                if(resultG[g][0]== 18){
                    gfGha = gfGha + resultG[g][2]
                    gcGha = gcGha + resultG[g][3]
                }else if (resultG[g][1]== 18){
                    gfGha = gfGha + resultG[g][3]
                    gcGha = gcGha + resultG[g][2]
                }

                if(resultG[g][4] == 1){
                    if(resultG[g][0]== 127 ){
                        GER = GER + 3  
                        
                    }else if (resultG[g][0]== 191 ){
                        USA = USA + 3
                    }else if (resultG[g][0]== 138){
                        POR = POR + 3
                    }else if (resultG[g][0]== 18){
                        GHA = GHA + 3
                    }    
                }else if (resultG[g][4] == 2){
                    if(resultG[g][1]== 127){
                        GER = GER + 3 
                    } else if (resultG[g][1]== 191){
                        USA = USA + 3
                    }else if (resultG[g][1]== 138){
                        POR = POR + 3
                    }else if (resultG[g][1]== 18){
                        GHA = GHA + 3
                    }  

                }else if (resultG[g][4] == 0){
                    if(resultG[g][1]== 127 || resultG[g][0]==127){
                        GER = GER  + 1  
                        
                    }if (resultG[g][0]== 191  || resultG[g][1]== 191 ){
                        USA = USA + 1

                    }if (resultG[g][1]== 138  || resultG[g][0]== 138){
                        POR = POR + 1

                    }if (resultG[g][1]== 18 || resultG[g][0]== 18 ){
                        GHA = GHA + 1
                    }
                }
                
            }
            for (var h = 0; h < resultH.length; h++){
                
                if(resultH[h][0]== 125){
                    gfBel = gfBel + resultH[h][2]
                    gcBel = gcBel + resultH[h][3]
                }else if (resultH[h][1]== 125){
                    gfBel = gfBel + resultH[h][3]
                    gcBel = gcBel + resultH[h][2]
                }
                if(resultH[h][0]== 1){
                    gfAlg = gfAlg + resultH[h][2]
                    gcAlg = gcAlg + resultH[h][3]
                }else if (resultH[h][1]== 1 ){
                    gfAlg = gfAlg + resultH[h][3]
                    gcAlg = gcAlg + resultH[h][2]
                }
                if(resultH[h][0]== 156){
                    gfRus = gfRus + resultH[h][2]
                    gcRus = gcRus + resultH[h][3]
                }else if (resultH[h][1]== 156 ){
                    gfRus = gfRus + resultH[h][3]
                    gcRus = gcRus + resultH[h][2]
                }
                if(resultH[h][0]== 74){
                    gfKor = gfKor + resultH[h][2]
                    gcCor = gcCor + resultH[h][3]
                }else if (resultH[h][1]== 74){
                    gfKor = gfKor + resultH[h][3]
                    gcCor = gcCor + resultH[h][2]
                }

                if(resultH[h][4] == 1){
                    if(resultH[h][0]== 125){
                        BEL = BEL + 3  
                        
                    }else if (resultH[h][0]== 1){
                        ALG = ALG  + 3
                    }else if (resultH[h][0]== 156){
                        RUS = RUS + 3
                    }else if (resultH[h][0]== 74){
                        KOR = KOR + 3
                    }    
                }else if (resultH[h][4] == 2){
                    if(resultH[h][1]== 125){
                        BEL = BEL + 3 
                    } else if (resultH[h][1]== 1 ){
                        ALG = ALG + 3
                    }else if (resultH[h][1]== 156){
                        RUS = RUS + 3
                    }else if (resultH[h][1]== 74 ){
                        KOR = KOR + 3
                    }  

                }else if (resultH[h][4] == 0){
                    if(resultH[h][1]== 125 || resultH[h][0]== 125){
                        BEL =  + 1  
                        
                    }if (resultH[h][0]== 1  || resultH[h][1]==  1){
                        ALG =  + 1

                    }if (resultH[h][1]==  156 || resultH[h][0]== 156 ){
                        RUS =  + 1
                    }if (resultH[h][1]== 74 || resultH[h][0]== 74 ){
                        KOR =  + 1
                    }
                }
                
            }

            var gdBra = gfBra - gcBra, gdCro = gfCro - gcCro, gdMex = gfMex - gcMex, gdCam = gfCam - gcCam  
            var gdEsp = gfEsp - gcEsp , gdNed = gfNed - gcNed , gdAut = gfAut - gcAut , gdChi = gfChi - gcChi
            var gdCol = gfCol - gcCol , gdGre = gfGre - gcGre , gdCiv = gfCiv - gcCiv , gdJPN = gfJPN - gcJPN
            var gdCrc = gfCrc - gcCrc , gdUru = gfUru - gcUru , gdIta = gfIta - gcIta , gdEng = gfEng - gcEng
            var gdFra = gfFra - gcFra , gdSui = gfSui - gcSui , gdEcu = gfEcu - gcEcu , gdHon = gfHon - gcHon
            var gdArg = gfArg - gcArg , gdNga = gfNga - gcNga , gdBih = gfBih - gcBih , gdIrn = gfIrn - gcIrn
            var gdGer = gfGer - gcGer , gdUsa = gfUsa - gcUsa , gdPor = gfPor - gcPor , gdGha = gfGha - gcGha
            var gdBel = gfBel - gcBel , gdAlg = gfAlg - gcAlg , gdRus = gfRus - gcRus , gdKor = gfKor - gcCor

            var grupA = []
            grupA = [{'EQUIPO':'BRASIL','CODE':BRA, 'GF':gfBra, 'GC':gcBra, 'GD': gdBra}, {'EQUIPO':'CROACIA','CODE':CRO, 'GF':gfCro, 'GC':gcCro, 'GD': gdCro }, {'EQUIPO':'MEXICO','CODE':MEX, 'GF':gfMex, 'GC':gcMex, 'GD': gdMex }, {'EQUIPO':'CAMERUN','CODE':CAM, 'GF':gfCam, 'GC':gcCam, 'GD': gdCam }]
            var grupB = [{'EQUIPO':'ESPAÑA','CODE':ESP, 'GF':gfEsp, 'GC':gcEsp, 'GD': gdEsp}, {'EQUIPO':'HOLANDA','CODE':NED, 'GF':gfNed, 'GC':gcNed, 'GD': gdNed }, {'EQUIPO':'AUSTRALIA','CODE':AUT, 'GF':gfAut, 'GC':gcAut, 'GD': gdAut }, {'EQUIPO':'CHILE','CODE':CHI, 'GF':gfChi , 'GC':gcChi, 'GD': gdChi }]
            var grupC = [{'EQUIPO':'COLOMBIA','CODE':COL , 'GF':gfCol, 'GC':gcCol, 'GD': gdCol}, {'EQUIPO':'GRECIA','CODE':GRE , 'GF':gfGre, 'GC':gcGre, 'GD': gdGre }, {'EQUIPO':'COSTA DE MARFIL','CODE':CIV , 'GF':gfCiv, 'GC':gcCiv, 'GD': gdCiv }, {'EQUIPO':'JAPON ','CODE':JPN , 'GF':gfJPN , 'GC':gcJPN , 'GD': gdJPN  }]
            var grupD = [{'EQUIPO':'COSTA RICA','CODE':CRC, 'GF':gfCrc, 'GC':gcCrc, 'GD': gdCrc}, {'EQUIPO':'URUGUAY','CODE': URU, 'GF':gfUru, 'GC':gcUru, 'GD': gdUru }, {'EQUIPO':'ITALIA','CODE':ITA , 'GF':gfIta, 'GC':gcIta, 'GD': gdIta }, {'EQUIPO':'INGLATERRA','CODE':ENG, 'GF':gfEng , 'GC':gcEng , 'GD': gdEng  }]
            var grupE = [{'EQUIPO':'FRANCIA','CODE':FRA , 'GF':gfFra, 'GC':gcFra, 'GD': gdFra}, {'EQUIPO':'SUIZA','CODE': SUI, 'GF':gfSui, 'GC':gcSui, 'GD': gdSui }, {'EQUIPO':' ECUADOR','CODE': ECU , 'GF':gfEcu, 'GC':gcEcu, 'GD': gdEcu }, {'EQUIPO':'HONDURAS','CODE': HON, 'GF':gfHon , 'GC':gcHon , 'GD': gdHon  }]
            var grupF = [{'EQUIPO':'ARGENTINA','CODE':ARG , 'GF':gfArg, 'GC':gcArg, 'GD': gdArg}, {'EQUIPO':'NIGERIA','CODE': NGA, 'GF':gfNga, 'GC':gcNga, 'GD': gdNga }, {'EQUIPO':'BOSNIA','CODE': BIH, 'GF':gfBih, 'GC':gcBih, 'GD': gdBih }, {'EQUIPO':'IRAN','CODE':IRN , 'GF':gfIrn , 'GC':gcIrn , 'GD': gdIrn  }]
            var grupG = [{'EQUIPO':'ALEMANIA','CODE':GER , 'GF':gfGer, 'GC':gcGer, 'GD': gdGer}, {'EQUIPO':'ESTADOS UNIDOS','CODE': USA, 'GF':gfUsa, 'GC':gcUsa, 'GD': gdUsa }, {'EQUIPO':'PORTUGAL','CODE':POR , 'GF':gfPor, 'GC':gcPor, 'GD': gdPor }, {'EQUIPO':'GHANA','CODE':GHA , 'GF':gfGha , 'GC':gcGha , 'GD': gdGha  }]
            var grupH = [{'EQUIPO':'BELGICA','CODE': BEL, 'GF':gfBel, 'GC':gcBel, 'GD': gdBel}, {'EQUIPO':'ARGELIA','CODE': ALG, 'GF':gfAlg, 'GC':gcAlg, 'GD': gdAlg }, {'EQUIPO':'RUSIA','CODE': RUS , 'GF':gfRus, 'GC':gcRus, 'GD': gdRus }, {'EQUIPO':'COREA DEL SUR','CODE':KOR , 'GF':gfKor , 'GC':gcCor , 'GD': gdKor  }]
            var gruposF =[grupA, grupB, grupC, grupD, grupE, grupF, grupG, grupH]
            for (var i = 0; i < gruposF.length; i++){
                gruposF[i].sort(function (b, a) {
                    if (a.CODE > b.CODE) {
                      return 1;
                    }
                    if (a.CODE < b.CODE) {
                      return -1;
                    }
                    // a must be equal to b
                    return 0;
                });

                //console.log(gruposF[i])

            }
            

            
            //console.log(grupA)
            res.json(gruposF)
            
            
            
        }


        getPartidos()

        
                
        //console.log(grupoA)
            
        
        //console.log(pos[0].GRUPO)
    })
})

//LISTA DE JUGADORES 
reqPerson.headers({
	"x-rapidapi-host": "montanaflynn-fifa-world-cup.p.rapidapi.com",
	"x-rapidapi-key": "da21fce768mshc2414d78b346310p12fdcbjsnccff2820202d",
	"accepts": "json"
});


reqPerson.end(function (res) {
    var arrJugadores = []
    if (res.error) throw new Error(res.error)
    arrJugadores = res.body
    app.get('/insertar/jugadores', (req, res) => {

        
        for (var i = 0; i < arrJugadores.length; i++){
            const insertJugadores = async() => {
                const text = 'INSERT INTO jugadores(id ,key ,name ,synonymus ,code ,born_at ,city_id ,region_id ,country_id ,nationality_id ,created_at ,updated_at ,person_id) VALUES($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13)'
                const values = [arrJugadores[i].id, arrJugadores[i].key, arrJugadores[i].name, arrJugadores[i].synonymus, arrJugadores[i].code, arrJugadores[i].born_at, arrJugadores[i].city_id, arrJugadores[i].region_id, arrJugadores[i].country_id ,arrJugadores[i].nationality_id, arrJugadores[i].created_at, arrJugadores[i].updated_at, arrJugadores[i].id] 

                var response = await pool.query(text, values)
            }

            insertJugadores()
        }
    })
})

//GOLES ANOTADOS

reqGoals.headers({
	"x-rapidapi-host": "montanaflynn-fifa-world-cup.p.rapidapi.com",
	"x-rapidapi-key": "da21fce768mshc2414d78b346310p12fdcbjsnccff2820202d",
	"accepts": "json"
});


reqGoals.end(function (res) {
    var arrGoles = []
    if (res.error) throw new Error(res.error)
    arrGoles = res.body
    app.get('/goles', (req, res) => {
        for (var i = 0; i < arrGoles.length; i++){
            const insertGoles = async() => {
                const text = 'INSERT INTO goles(id, person_id,game_id,team_id,minute,score1,score2,penalty,owngoal,created_at,updated_at) VALUES($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11)'
                const values = [arrGoles[i].id, arrGoles[i].person_id, arrGoles[i].game_id, arrGoles[i].team_id, arrGoles[i].minute, arrGoles[i].score1, arrGoles[i].score2 ,arrGoles[i].penalty, arrGoles[i].owngoal , arrGoles[i].created_at, arrGoles[i].updated_at]  

                var response = await pool.query(text, values)
            }

            insertGoles()    
        }
        

    })
    

    app.get('/tabla/goles',(req, res) => {
        const getGolesAnotados = async () => {
            var text = 'SELECT count(goles.person_id) AS "GOLES", jugadores.name AS "NOMBRE", goles.team_id FROM goles INNER JOIN jugadores ON goles.person_id=jugadores.person_id GROUP BY jugadores.name, goles.team_id ORDER BY goles.team_id ASC'
            


            var response = await pool.query(text) 
            //console.log(response.rows)
            var golJugadores = response.rows
            var maxGoleadores = []
            for (var i = 0; i < golJugadores.length; i++){
                maxGoleadores.push([golJugadores[i].GOLES ,golJugadores[i].NOMBRE])
        
            }
            maxGoleadores.sort()
        
            //console.log(maxGoleadores)
            res.json(maxGoleadores)
            
        }

        getGolesAnotados()

    })

    //Buscar por minutos
    app.get('/busqueda/gol/:min/:con', (req, res) =>{
        const minGol = async () => {
            var con = req.params.con
            const min = req.params.min
            var text = 'SELECT count(goles.person_id) AS "GOLES", jugadores.name AS "NOMBRE", goles.minute FROM goles INNER JOIN jugadores ON goles.person_id=jugadores.person_id GROUP BY jugadores.name, goles.minute ORDER BY goles.minute ASC'
            var response = await pool.query(text) 
            var golMin = response.rows
            var golAnotado = []
            for(var i = 0; i < golMin.length; i++){
                
                if (con == "igual"){
                    if (min == golMin[i].minute){
                        golAnotado.push([golMin[i].minute, golMin[i].NOMBRE])
                    }
                }if (con == "menor"){
                    if (min <= golMin[i].minute){
                        golAnotado.push([golMin[i].minute, golMin[i].NOMBRE])
                    }
                }else if(con == "mayor"){
                    if (min >= golMin[i].minute){
                        golAnotado.push([golMin[i].minute, golMin[i].NOMBRE])
                    }
                }
                
                
            }

            console.log(golAnotado)
            res.json(golAnotado)
            
        }

        minGol()
    })
})




app.listen(port, () => {
    console.log(`API REST corriendo correctamente en http://localhost:${port}`)
})