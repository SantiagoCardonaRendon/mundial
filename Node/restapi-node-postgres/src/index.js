const express = require('express')
const app = express()

//Middlewares
app.use(express.json())
app.use(express.urlencoded({extended : false}))



//Las rutas
app.use(require('./routes/index'))


//El puerto de salida
app.listen(4000)
console.log('Server on port 4000')
