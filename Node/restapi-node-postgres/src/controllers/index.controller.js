const {Pool} = require('pg')
 
//LA CONEXION A LA BASE DE DATOS
const pool = new Pool({
    user : 'postgres',
    host : 'localhost',
    password : 'postgresql',
    database : 'firstapi',
    port : '5432'
})

//BUSCA TODOS LOS USUARIOS
const getUsers = async (req, res) => {
    const response = await pool.query('SELECT * FROM users')
    console.log(response.rows)
    res.status(200).json(response.rows)
}

//BUSCA UN USUARIO POR UN ID EN PARTICULAR
const getUserById = async (req, res) => {
    const id = req.params.id
    const response =  await pool.query('SELECT * FROM users WHERE id = $1', [ id ])
    //const name = req.params.name
    //const response =  await pool.query('SELECT * FROM users WHERE name = $1', [ name ])
    res.json(response.rows)
}

//CREA UN USUARIO EN LA BASE DE DATOS
const createUser = async(req, res) => {

    const {name, email} = req.body
    const response = await pool.query('INSERT INTO users (name, email) VALUES ($1, $2)', [name, email])
    console.log(response)
    res.send('User created')
    res.json({
        message : 'User Added Sucessfully',
        body : {
            user : {name, email}
        }
    })
    
}

//ELIMINA UN USUARIO DE LA BASE DE DATOS CON EL ID
const deleteUser = async(req, res) => {
    const id = req.params.id
    const response = await pool.query('DELETE FROM users WHERE id = $1', [id])
    console.log(response)
    res.json(`Ùser ${id} delete successfully`) 
}

//MODIFICAR UN USUARIO
const updateUser = async(req, res) => {
    const id = req.params.id
    const { name, email } = req.body
    const response = await pool.query('UPDATE users SET name = $1, email = $2 WHERE id = $3', [
        name, 
        email,
        id
    ])
    console.log(response)
    res.json('User Update Successfully')
}



//SE EXPORTAN LOS MODULOS DE LAS FUNCIONES
module.exports = {
    getUsers, 
    createUser,
    getUserById,
    updateUser,
    deleteUser
}