import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../services/auth.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-singup',
  templateUrl: './singup.component.html',
  styleUrls: ['./singup.component.css']
})
export class SingupComponent implements OnInit {
  user = {}

  constructor(
    private authService : AuthService,
    private router : Router
    ) {
    console.log(authService)
  }

  

  ngOnInit() {
  }

  singUp(){
    this.authService.singUp(this.user)
    .subscribe(
      res =>{
        //console.log(res)
        
        localStorage.setItem('token',res.token)
        this.router.navigate(['/home'])
      },
      err =>{
        console.log(err)
      } 

    )
    
  
  }

  

}
