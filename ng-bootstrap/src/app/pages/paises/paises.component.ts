import { Component, OnInit } from '@angular/core';
import {DataService} from "app/data.service"; 
import { Title } from '@angular/platform-browser';



@Component({
  selector: 'app-paises',
  templateUrl: './paises.component.html',
  styleUrls: ['./paises.component.css']
})
export class PaisesComponent implements OnInit {
  paises = {}
  
  p: number = 1;
  collection: any[] = [];


  constructor(private dataService: DataService){
    this.dataService.getData2().subscribe(data => {
      this.paises = data;

      console.log(this.paises[0][0])

    });
  }  

  ngOnInit() {
  }


}
