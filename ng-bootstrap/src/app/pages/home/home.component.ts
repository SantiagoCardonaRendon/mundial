import {OnInit, Component} from "@angular/core";
import {Router} from "@angular/router";



@Component(
    {
    selector: 'home',
    templateUrl: './home.component.html'
    }
)
export class HomeComponent implements OnInit {
    constructor( private router: Router) { }

    ngOnInit() {}
    activeRoute(routename: string): boolean{
        return this.router.url.indexOf(routename) > -1;
    }
}