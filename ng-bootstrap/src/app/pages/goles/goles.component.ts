import { Component, OnInit } from '@angular/core';
import {DataService} from "app/data.service"; 

@Component({
  selector: 'app-goles',
  templateUrl: './goles.component.html',
  styleUrls: ['./goles.component.css']
})
export class GolesComponent implements OnInit {
  goles = {}; 
  p: number = 1;
  collection: any[] = [];
  constructor(private dataService: DataService){
    this.dataService.getData().subscribe(data => {
      this.goles = data;


    });
  }  

  ngOnInit() {
  }

}
