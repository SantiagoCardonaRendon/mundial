import { Component, OnInit } from '@angular/core';
import {DataService} from "app/data.service"; 

@Component({
  selector: 'app-mundial',
  templateUrl: './mundial.component.html',
  styleUrls: ['./mundial.component.css']
})
export class MundialComponent implements OnInit {
  encuentros = {};


  constructor(private dataService: DataService){
    this.dataService.getData3().subscribe(data => {
      this.encuentros = data;

        console.log(this.encuentros)
      }
    )  
  } 
  

  ngOnInit() {
  }

}
