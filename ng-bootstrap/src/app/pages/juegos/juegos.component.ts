import { Component, OnInit } from '@angular/core';
import {DataService} from "app/data.service"; 

@Component({
  selector: 'app-juegos',
  templateUrl: './juegos.component.html',
  styleUrls: ['./juegos.component.css']
})
export class JuegosComponent implements OnInit {
  
  partidos = {}; 
  constructor(private dataService: DataService){
    this.dataService.getData1().subscribe(data => {
      this.partidos = data;

      console.log(this.partidos[0]);
    });
  }

  ngOnInit() {
  }

}
