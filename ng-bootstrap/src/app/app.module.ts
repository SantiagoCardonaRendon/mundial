import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { HttpClientModule} from '@angular/common/http';


import { AppComponent } from './app.component';
import {Topnavbar} from "./components/topnavbar/topnavbar.component";
import {Navigation} from "./components/navigation/navigation.component";
import {Routes ,RouterModule} from "@angular/router";
import {appRoutes} from "./app.routes";
import {HomeComponent} from "./pages/home/home.component";
import { PaisesComponent } from './pages/paises/paises.component';
import { MundialComponent } from './pages/mundial/mundial.component';
import { JuegosComponent } from './pages/juegos/juegos.component';
import { GolesComponent } from './pages/goles/goles.component'



import {DataService} from 'app/data.service';
import {AuthService} from 'app/services/auth.service';
import {AuthGuard} from 'app/services/auth.guard'


import { NgxPaginationModule} from 'ngx-pagination';
import { SingupComponent } from './components/singup/singup.component';
import { SinginComponent } from './components/singin/singin.component';
import { TaskComponent } from './components/task/task.component';
import { PrivateComponent } from './components/private/private.component'






@NgModule({
  declarations: [
    AppComponent,
    Navigation,
    Topnavbar,
    HomeComponent,
    PaisesComponent,
    MundialComponent,
    JuegosComponent,
    GolesComponent,
    SingupComponent,
    SinginComponent,
    TaskComponent,
    PrivateComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(appRoutes),
    HttpClientModule, 
    NgxPaginationModule,
  ],
  providers: [DataService, AuthService, AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
