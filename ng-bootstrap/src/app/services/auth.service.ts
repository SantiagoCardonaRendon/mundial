import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Injectable()


export class AuthService {
  private URL = 'http://localhost:3001'

  constructor(private httpClient: HttpClient){}

  singUp(user){
    return this.httpClient.post<any>(this.URL + '/singup', user)

  }

  singIn(user){
    return this.httpClient.post<any>(this.URL + '/singin', user)
  }

  loggedIn(){
    
    if (localStorage.getItem('token')){
      return true;
    }
    else{
      return false;
    }
    
    

  }
  

}
