import {HomeComponent} from "./pages/home/home.component";
import { PaisesComponent } from "./pages/paises/paises.component";
import { MundialComponent } from "./pages/mundial/mundial.component";
import { JuegosComponent } from "./pages/juegos/juegos.component";
import { GolesComponent } from "./pages/goles/goles.component";

import { SingupComponent } from './components/singup/singup.component';
import { SinginComponent } from './components/singin/singin.component';
import { TaskComponent } from './components/task/task.component';
import { PrivateComponent } from './components/private/private.component'
import { AuthGuard } from "./services/auth.guard";

export const appRoutes=[
    {
        path:'',
        redirectTo:'singup',
        pathMatch:'full'
     
    },
    {
        path: 'home',
        pathMatch:'full',
        component: HomeComponent
        
        
    },
    {
        path: 'others',
        loadChildren:'./pages/others/others.module#OthersModule',
    },
    {
        path: 'paises',
        component: PaisesComponent,
        pathMatch:'full',
        
    },
    {
        path: 'mundial',
        component: MundialComponent
    },
    {
        path: 'juegos',
        component: JuegosComponent
    },
    {
        path: 'goles',
        component: GolesComponent,
        canActive:[AuthGuard]
    },
    {
        path : 'task',
        component: TaskComponent
    
    },
    {
        path : 'private',
        component: PrivateComponent
    
    },
    {
        path : 'singup',
        component: SingupComponent
    
    },
    {
        path : 'singin',
        component: SinginComponent,
       
    
    },
    
];